const someArray = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]
const someArray2 = ["1", "2", "3", "sea", "user", 23]

function getArray(array, parent) {
  parent = document.body
  let ul = document.createElement("ul")
  parent.appendChild(ul)

  array.forEach(function (item) {
    const li = document.createElement("li")
    li.appendChild(document.createTextNode(item))
    ul.appendChild(li)
  })
}

getArray(someArray)
getArray(someArray2)
